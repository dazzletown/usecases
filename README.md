# Use cases for [Dazzle](https://dazzle.town/).

The [issue tracker](https://gitlab.com/dazzletown/usecases/-/issues) for the
[use case collection](https://docs.google.com/spreadsheets/d/1OiQYpASCFJUe5Lu1lUoYYtCeoiJwjuJN8Rafuv0V0zI/edit#gid=0)
we are working on.

We welcome your contribution, but you first need to sign the Intellectual
Property Declaration [here](https://docs.google.com/forms/d/e/1FAIpQLSf3g3JvtdY-dqKtQoInVt2-5JipjdxYQc2FN2SbzsR5-tafOw/viewform).
Use the same e-mail address as you use for your account here on Gitlab.

